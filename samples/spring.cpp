#include <stdexcept>
#include <vector>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../src/graphics/Drawer.h"
#include "../src/physics/Particle.h"
#include "../src/physics/ElasticForce.h"
#include "../src/physics/ElasticMediator.h"
#include "../src/physics/Force.h"


using namespace impact;

int main() {
  Vector3 p1(0.0, 2.0, 1.0);
  Vector3 p2(7.0, 2.0, 1.0);
  Particle part1 = Particle::Builder(1.0).position(p1).build();
  Particle part2 = Particle::Builder(1.0).position(p2).build();


  ElasticForce spring(12.0, 8.0);
  ElasticMediator em(spring, part1, part2);

//  part1.applyForce(std::make_shared<Force>(Vector3(1.0, 0.0, 0.0)));

  Drawer draw;
  std::pair<std::string, Particle*> paio1 = std::make_pair("../models/sphere_uv.ply", &part1);
  std::pair<std::string, Particle*> paio2 = std::make_pair("../models/sphere_uv.ply", &part2);
  draw.addPair(paio1);
  draw.addPair(paio2);

  const real INTEGRATION_TIME = 0.01;

  while(draw.renderOnScreen()){
    part1.integrate(INTEGRATION_TIME);
    part2.integrate(INTEGRATION_TIME);
    part1.updateMessage(&em);
    part2.updateMessage(&em);
  }
}
