// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_GRAPHICS_SHADERMANAGER_H_
#define SRC_GRAPHICS_SHADERMANAGER_H_

#include <vector>
#include <iostream>

//#define GLEW_STATIC
#include <GL/glew.h>

namespace impact {

class ShaderManager {

  public:
    ShaderManager();
    virtual ~ShaderManager();
    void init();
    void addShader(GLenum shaderType, std::string path);
    void finalize();
    void enable();
    GLint getAttribLocation(std::string attributeName);
    GLint getUniformLocation(std::string uniformName);
    void printActiveUniforms();
//    void addFeedbackTransform(std::string nameVar);

  private:
    typedef std::vector<GLuint> ShaderObjList_;
    ShaderObjList_ shaderObjList_;
    GLuint shaderProg_;
    std::string getShaderTypeString(GLenum shaderType);
    GLuint loadShaderFromFile(std::string path, GLenum shaderType);
};

}

#endif   // SRC_GRAPHICS_SHADERMANAGER_H_
