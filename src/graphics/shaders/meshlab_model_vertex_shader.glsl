#version 330

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;

// Output data ; will be interpolated for each fragment.
out vec4 color;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;

void main(void)
{
    gl_Position = MVP * vec4(position, 1);

    // Compute the vertex's normal in camera space
    vec3 normal_cameraspace = normalize(vec4( V*M * vec4(normal,0)).xyz);
    // Vector from the vertex (in camera space) to the camera (which is at the origin)
    vec3 cameraVector = normalize(vec3(0, 0, 0) - vec3(V*M * vec4(position, 1)).xyz);

    // Compute the angle between the two vectors
    float cosTheta = clamp( dot( normal_cameraspace, cameraVector ), 0,1 );

    // The coefficient will create a nice looking shining effect.
    // Also, we shouldn't modify the alpha channel value.
    vec4 in_color = vec4(0.8, 0.8, 0.8, 1.0);
    color = vec4(0.3 * in_color.rgb + cosTheta * in_color.rgb, in_color.a);
}
