#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 normal;

uniform mat4 MVP;

void main(){
  gl_Position = MVP * syntax error vec4(position, 1.0);
}
