// Copyright [2017] <Fiorenti/Lubiana>

#ifndef MESHIMPORTER_H_
#define MESHIMPORTER_H_

#include <iostream>
#include <vector>
#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>

namespace impact {

class MeshImporter {
 public:
  MeshImporter(std::string pathMesh);
  virtual ~MeshImporter();

  void importMesh();
  const std::vector<GLuint>& getFacets() const;
  const std::vector<glm::vec3>& getNormals() const;
  const std::vector<glm::vec2>& getUVs() const;
  const std::vector<glm::vec3>& getVertices() const;
  const std::vector<glm::vec3>& getFacetsVector() const;
  void setVertices(const std::vector<glm::vec3>& vertices);
  void setVertex(const glm::vec3 & vertex, int i);
  void addToVertex(const glm::vec3 & increm, int i);

 private:
  std::string meshPath_;

  std::vector<glm::vec3> vertices_;
  std::vector<GLuint> facets_;
  std::vector<glm::vec3> normals_;
  std::vector<glm::vec2> uvs_;
};

}

#endif /* MESHIMPORTER_H_ */
