// Copyright [2017] <Fiorenti/Lubiana>
#include "Drawer.h"
#include <stdio.h>
#include <stdlib.h>
#include <glm/gtc/matrix_transform.hpp>
#include <stdexcept>

#define SCREEN_FRAMEBUFFER 0

using namespace impact;

// Public methods

Drawer::Drawer(const int &screenWidth, const int &screenHeight, const std::string &windowName) {
  // Initialise GLFW
  if( !glfwInit() )
  {
    throw std::runtime_error("Failed to initialize GLFW");
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  openWindow(screenWidth, screenHeight, windowName);

  // Initialize GLEW
  glewExperimental = true; // Needed for core profile
  if (glewInit() != GLEW_OK) {
    glfwTerminate();
    throw std::runtime_error("Failed to initialize GLEW");
  }

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(_window, GLFW_STICKY_KEYS, GL_TRUE);

  // Dark blue background
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  // Enable depth test
  glEnable(GL_DEPTH_TEST);
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc(GL_LESS);

  glGenVertexArrays(1, &_vertexArrayID);
  glBindVertexArray(_vertexArrayID);

  // Create and compile our GLSL program from the shaders
  setUpShaders();
  _shaderManager.enable();
}

void Drawer::openWindow(const int &screenWidth, const int &screenHeight, const std::string &windowName)
{
  // Open a window and create its OpenGL context
  _window = glfwCreateWindow( screenWidth, screenHeight, windowName.c_str(), NULL, NULL);
  if( _window == NULL ){
    glfwTerminate();
    throw std::runtime_error("Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.");
  }
  glfwMakeContextCurrent(_window);
}

Drawer::~Drawer()
{
  // Cleanup VBO and shader
  for(unsigned long i = 0; i < _meshWrapperVector.size(); i++) {
    glDeleteBuffers(1, &_meshWrapperVector[i]._buffContainer._vertexBuffer);
    glDeleteBuffers(1, &_meshWrapperVector[i]._buffContainer._normalBuffer);
    glDeleteBuffers(1, &_meshWrapperVector[i]._buffContainer._uvBuffer);
  }
  glDeleteVertexArrays(1, &_vertexArrayID);

  // Close OpenGL window and terminate GLFW
  glfwTerminate();
}

void Drawer::setPairs(std::vector<std::pair<std::string, Particle*>> &meshParticlePairs) {
  _meshParticlePairs = meshParticlePairs;
  for(auto pair: _meshParticlePairs) {
    addMesh(pair.first);
  }
}

void Drawer::addPair(std::pair<std::string, Particle*> &meshParticle) {
  addMesh(meshParticle.first);
  _meshParticlePairs.push_back(meshParticle);
}

bool Drawer::renderOnScreen() {
  if(glfwWindowShouldClose(_window)) {
    return false;
  }
  if(glfwGetKey(_window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    glfwDestroyWindow(_window);
    return false;
  }

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for(unsigned long i = 0; i < _meshWrapperVector.size(); i++) {
    glm::mat4 posMat = glm::mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
                                 _meshParticlePairs[i].second->getPosition().getGlmVec3().x,
                                 _meshParticlePairs[i].second->getPosition().getGlmVec3().y,
                                 _meshParticlePairs[i].second->getPosition().getGlmVec3().z, 1);

    simpleRendering(_meshWrapperVector[i], posMat);
  }
  glfwSwapBuffers(_window);
  glfwPollEvents();

  return true;
}

void Drawer::closeWindow()
{
  glfwSetWindowShouldClose(_window, GL_TRUE);
}

void Drawer::setUpShaders() {
  std::string shadersFolder = "../src/graphics/shaders/";

  _shaderManager.init();
  _shaderManager.addShader(GL_VERTEX_SHADER, shadersFolder+"meshlab_model_vertex_shader.glsl");
  _shaderManager.addShader(GL_FRAGMENT_SHADER, shadersFolder+"meshlab_model_fragment_shader.glsl");
  _shaderManager.finalize();
}

void Drawer::addMesh(const std::string &meshPath)
{
  meshWrap mWrapper(meshPath);
  createBuffers(mWrapper._buffContainer, mWrapper._meshImporter);

  _meshWrapperVector.push_back(mWrapper);
}

void Drawer::createBuffers(buffContainer &bc, MeshImporter &meshImporter){
  glGenBuffers(1, &bc._vertexBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, bc._vertexBuffer);
  glBufferData(GL_ARRAY_BUFFER, static_cast<long>(meshImporter.getVertices().size() * sizeof(glm::vec3)), &meshImporter.getVertices()[0], GL_STATIC_DRAW);

  glGenBuffers(1, &bc._uvBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, bc._uvBuffer);
  glBufferData(GL_ARRAY_BUFFER, static_cast<long>(meshImporter.getUVs().size() * sizeof(glm::vec2)), &meshImporter.getUVs()[0], GL_STATIC_DRAW);

  glGenBuffers(1, &bc._normalBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, bc._normalBuffer);
  glBufferData(GL_ARRAY_BUFFER, static_cast<long>(meshImporter.getNormals().size() * sizeof(glm::vec3)), &meshImporter.getNormals()[0], GL_STATIC_DRAW);

  glGenBuffers(1, &bc._facetBuffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bc._facetBuffer);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<long>(meshImporter.getFacets().size() * sizeof(GLuint)), &meshImporter.getFacets()[0], GL_STATIC_DRAW);
}


void Drawer::simpleRendering(meshWrap &mw, glm::mat4 &modelMatrix)
{
  int width, height;
  glfwGetWindowSize(_window, &width, &height);
  glViewport(0, 0, width, height);

  GLint MatrixID = _shaderManager.getUniformLocation("MVP");
  GLint View_ID = _shaderManager.getUniformLocation("V");
  GLint Model_ID = _shaderManager.getUniformLocation("M");

  glm::mat4 Projection = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);
  glm::mat4 View       = glm::lookAt(glm::vec3(30,30,30), // Camera is at this point, in World Space
                                     glm::vec3(0,0,0), // and looks at the origin
                                     glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
                                     );
  glm::mat4 MVP        = Projection * View * modelMatrix; // Remember, matrix multiplication is the other way around

  glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
  glUniformMatrix4fv(View_ID, 1, GL_FALSE, &View[0][0]);
  glUniformMatrix4fv(Model_ID, 1, GL_FALSE, &modelMatrix[0][0]);

  // 1st attribute buffer : vertex
  GLuint posAttrib_ID = static_cast<GLuint>(_shaderManager.getAttribLocation("position"));
  glEnableVertexAttribArray(posAttrib_ID);
  glBindBuffer(GL_ARRAY_BUFFER, mw._buffContainer._vertexBuffer);
  glVertexAttribPointer(posAttrib_ID, 3, GL_FLOAT, GL_FALSE, 0, 0);

  // 2nd attribute buffer : UVs
  GLuint uvAttr_ID = static_cast<GLuint>(_shaderManager.getAttribLocation("vertexUV"));
  glEnableVertexAttribArray(uvAttr_ID);
  glBindBuffer(GL_ARRAY_BUFFER, mw._buffContainer._uvBuffer);
  glVertexAttribPointer(uvAttr_ID, 2, GL_FLOAT, GL_FALSE, 0, 0);

  // 3rd attribute buffer : normals
  GLuint normalAttr_ID = static_cast<GLuint>(_shaderManager.getAttribLocation("normal"));
  glEnableVertexAttribArray(normalAttr_ID);
  glBindBuffer(GL_ARRAY_BUFFER, mw._buffContainer._normalBuffer);
  glVertexAttribPointer(normalAttr_ID, 3, GL_FLOAT, GL_FALSE, 0, 0);

  // Index buffer
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mw._buffContainer._facetBuffer);

  // Draw the triangle !
//    glDrawElements(GL_TRIANGLES, _meshWrapperVector[0]._meshImporter.getFacets().size(), GL_UNSIGNED_INT, 0);
  glDrawArrays(GL_TRIANGLES, 0, static_cast<int>(mw._meshImporter.getFacets().size()));

//  glUseProgram(0); //unbind the shader
  glBindFramebuffer(GL_FRAMEBUFFER, 0); //unbind the FBO

  glDisableVertexAttribArray(posAttrib_ID);
  glDisableVertexAttribArray(uvAttr_ID);
  glDisableVertexAttribArray(normalAttr_ID);
}
