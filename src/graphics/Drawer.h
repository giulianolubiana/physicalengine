// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_GRAPHICS_DRAWER_H_
#define SRC_GRAPHICS_DRAWER_H_

#include "ShaderManager.h"
#include "MeshImporter.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <string>
#include "../physics/Particle.h"

namespace impact {

struct buffContainer {
  GLuint _vertexBuffer;
  GLuint _uvBuffer;
  GLuint _normalBuffer;
  GLuint _facetBuffer;
};

struct meshWrap {
  meshWrap(std::string meshPath):
    _meshImporter(meshPath)
  {
    _meshImporter.importMesh();
  }

  MeshImporter _meshImporter;
  buffContainer _buffContainer;
};

class Drawer {
 public:
  Drawer(const int &screenWidth=800, const int &screenHeight=600, const std::string &windowName="Test");
  ~Drawer();
  void setPairs(std::vector<std::pair<std::string, Particle*>> &meshParticlePairs);
  void addPair(std::pair<std::string, Particle *> &meshParticle);
  bool renderOnScreen();
  void closeWindow();

private:
  GLFWwindow* _window;
  ShaderManager _shaderManager;
  std::vector<meshWrap> _meshWrapperVector;
  std::vector<std::pair<std::string, Particle*>> _meshParticlePairs;
  GLuint _vertexArrayID;

  void openWindow(const int &screenWidth, const int &screenHeight, const std::string &windowName);
  void setUpShaders();
  void createBuffers(buffContainer &bc, MeshImporter &meshImporter);
  void simpleRendering(meshWrap &mw, glm::mat4 &modelMatrix);
  void bindAndClearFramebuffer(GLuint Framebuffer, glm::vec3 clearColor);
  void endRendering();
  void addMesh(const std::string &meshPath);
};

}

#endif   // SRC_GRAPHICS_DRAWER_H_
