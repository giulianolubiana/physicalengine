// Copyright [2017] <Fiorenti/Lubiana>

#include "MeshImporter.h"

#include <fstream>
#include <sstream>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

using namespace impact;

MeshImporter::MeshImporter(std::string meshPath) {
  meshPath_ = meshPath;
}

MeshImporter::~MeshImporter() {
}

void MeshImporter::importMesh() {
  std::ifstream in(meshPath_.c_str(), std::ios::in);
  if (!in) {
    throw std::invalid_argument("Cannot open mesh " + meshPath_);
  }

  /* Carico la mesh con assimp */
  Assimp::Importer importer;

  const aiScene* scene = importer.ReadFile(meshPath_, 0/*aiProcess_JoinIdenticalVertices | aiProcess_SortByPType*/);
  if(!scene) {
    throw std::runtime_error(importer.GetErrorString());
  }
  const aiMesh* mesh = scene->mMeshes[0]; // In this simple example code we always use the 1rst mesh (in OBJ files there is often only one anyway)

  // Fill vertices positions
  vertices_.reserve(mesh->mNumVertices);
  for(unsigned int i=0; i<mesh->mNumVertices; i++){
    aiVector3D pos = mesh->mVertices[i];
    vertices_.push_back(glm::vec3(pos.x, pos.y, pos.z));
  }

  if(mesh->HasTextureCoords(0)){
    // Fill vertices texture coordinates
    uvs_.reserve(mesh->mNumVertices);
    for(unsigned int i=0; i<mesh->mNumVertices; i++){
      aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
      uvs_.push_back(glm::vec2(UVW.x, UVW.y));
    }
  }else{
    std::cerr << "Warning: Mesh "+meshPath_+" has no UV coordinates!" << std::endl;
  }
  // Fill vertices normals
  if(mesh->HasNormals()){
    normals_.reserve(mesh->mNumVertices);
    for(unsigned int i=0; i<mesh->mNumVertices; i++){
      aiVector3D n = mesh->mNormals[i];
      normals_.push_back(glm::vec3(n.x, n.y, n.z));
    }
  }else{
    std::cerr << "Warning: Mesh "+meshPath_+" has no Normals!" << std::endl;
  }

  // Fill face indices
  facets_.reserve(3*mesh->mNumFaces);
  for (unsigned int i=0; i<mesh->mNumFaces; i++){
    // Assume the model has only triangles.
    facets_.push_back(mesh->mFaces[i].mIndices[0]);
    facets_.push_back(mesh->mFaces[i].mIndices[1]);
    facets_.push_back(mesh->mFaces[i].mIndices[2]);
  }
}

const std::vector<GLuint>& MeshImporter::getFacets() const {
  return facets_;
}

const std::vector<glm::vec3>& MeshImporter::getNormals() const {
  return normals_;
}

const std::vector<glm::vec2>& MeshImporter::getUVs() const {
    return uvs_;
}

const std::vector<glm::vec3>& MeshImporter::getVertices() const {
  return vertices_;
}
