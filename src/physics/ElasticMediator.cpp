// Copyright [2017] <Fiorenti/Lubiana>

#include "ElasticMediator.h"

using namespace impact;

ElasticMediator::ElasticMediator(ElasticForce& ef, Particle& p1, Particle& p2):
  _ef2(new ElasticForce(ef)),
  _p1(p1),
  _p2(p2)
{
  _ef1 = std::make_shared<ElasticForce>(ef);
  _p1.applyForce(_ef1);
  _p2.applyForce(_ef2);
}


void ElasticMediator::sendMessage(Collegue* c) const {
  if (!correctCollegue(c))
    throw std::invalid_argument("This colleague isn't subscribed to this mediator");

  Vector3 d = _p1.distance(_p2);
  _ef1->updateLength(d);
  d.invert();
  _ef2->updateLength(d);
}


bool ElasticMediator::correctCollegue(const Collegue* const c) const {
  return (c == &_p1 || c == &_p2);
}
