// Copyright [2017] <Fiorenti/Lubiana>

#include "Vector3.h"
#include <math.h>
#include <string.h>
#include <iostream>
#include "Utility.h"

using namespace impact;

Vector3::Vector3():
    _x{0},
    _y{0},
    _z{0}
{}

Vector3::Vector3(real x, real y, real z):
    _x{x},
    _y{y},
    _z{z}
{}

void Vector3::invert() {
  _x = -_x;
  _y = -_y;
  _z = -_z;
}

void Vector3::normalize() {
  real mag = magnitude();
  if (mag != 0)
    *this = (*this) * (1/magnitude());
}

real Vector3::magnitude() const {
  return sqrt(squareMagnitude());
}

real Vector3::squareMagnitude() const {
  return _x*_x+_y*_y+_z*_z;
}

bool Vector3::overlap(const Vector3& v) const {
  return _x == v._x && _y == v._y && _z == v._z;
}

Vector3 impact::operator+(const Vector3& v1, const Vector3& v2) {
  return Vector3(v1._x + v2._x, v1._y + v2._y, v1._z + v2._z);
}

void Vector3::operator+=(const Vector3& v2) {
  _x += v2._x;
  _y += v2._y;
  _z += v2._z;
}

void Vector3::operator-=(const Vector3& v) {
  _x -= v._x;
  _y -= v._y;
  _z -= v._z;
}

Vector3 impact::operator-(const Vector3& v1, const Vector3& v2) {
  return Vector3(v1._x - v2._x, v1._y - v2._y, v1._z - v2._z);
}

Vector3 Vector3::componentProduct(const Vector3& v) const {
  return Vector3(_x * v._x, _y * v._y, _z * v._z);
}

real impact::operator*(const Vector3& v1, const Vector3& v2) {
  return v1._x * v2._x + v1._y * v2._y + v1._z * v2._z;
}

Vector3 Vector3::vectorProduct(const Vector3& v) const {
  return Vector3(_y * v._z - _z * v._y,
                 _z * v._x - _x * v._z,
                 _x * v._y - _y * v._x);
}

std::string Vector3::toString(size_t n) const{
    return "(" +
            utility::realFormat(_x, n) + ", " +
            utility::realFormat(_y, n) + ", " +
            utility::realFormat(_z, n) +
        ")";
}

glm::vec3 Vector3::getGlmVec3() const {
  return glm::vec3(_x, _y, _z);
}

Vector3 impact::operator*(const Vector3 &v, const real scalar)
{
  return Vector3(v._x * scalar, v._y * scalar, v._z * scalar);
}

Vector3 impact::operator*(const real scalar, const Vector3 &v)
{
  return v * scalar;
}

Vector3 impact::operator/(const impact::Vector3 &v, const real scalar)
{
  return Vector3(v._x / scalar, v._y / scalar, v._z / scalar);
}
