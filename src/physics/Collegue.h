// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_COLLEGUE_H_
#define SRC_COLLEGUE_H_

#include "Mediator.h"
#include "Vector3.h"

namespace impact {
class Mediator;
class Collegue {
public:
  virtual ~Collegue() = 0;
  virtual void updateMessage(const Mediator*) = 0;
};

inline Collegue::~Collegue(){}
}

#endif  // SRC_COLLEGUE_H_
