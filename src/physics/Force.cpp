// Copyright [2017] <Fiorenti/Lubiana>
#ifndef SRC_FORCE_H
#define SRC_FORCE_H

#include "Force.h"

using namespace impact;

Force::Force(Vector3 vResForce) :
    _vResForce(vResForce)
{}

Vector3 const Force::getResultingForce() const {
  return _vResForce;
}

#endif   // SRC_FORCE_H
