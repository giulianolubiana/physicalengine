#include "BarMediator.h"
#include <stdexcept>

using namespace impact;

BarMediator::BarMediator(Particle &p1, Particle &p2) :
  _bf1(new BarForce(p1.distance(p2), p1.getReducedMass(p2))),
  _bf2(new BarForce(p2.distance(p1), p2.getReducedMass(p1))),
  _p1(p1),
  _p2(p2)
{
  _p1.applyForce(_bf1);
  _p2.applyForce(_bf2);
}

void BarMediator::sendMessage(Collegue* c) const {
  if (!correctCollegue(c))
    throw std::invalid_argument("This colleague isn't subscribed to this mediator");

  Vector3 d = _p1.distance(_p2);
  _bf1->updateDistance(d);
  d.invert();
  _bf2->updateDistance(d);
  
  _bf1->updateVelocity(_p1.velocityDifference(_p2));
  _bf2->updateVelocity(_p2.velocityDifference(_p1));
}


bool BarMediator::correctCollegue(const Collegue* const c) const {
  return c == &_p1 || c == &_p2;
}
