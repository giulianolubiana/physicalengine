// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_ABSTRACT_FORCE_H_
#define SRC_ABSTRACT_FORCE_H_

#include "Vector3.h"

namespace impact {

class AbstractForce {

public:
  virtual ~AbstractForce() = 0;
  virtual Vector3 const getResultingForce() const = 0;
};
 
 inline AbstractForce::~AbstractForce(){} 
}

#endif //  SRC_ABSTRACT_FORCE_H_
