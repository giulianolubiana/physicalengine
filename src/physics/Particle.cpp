// Copyright [2017] <Fiorenti/Lubiana>

#include "Particle.h"
#include "Utility.h"
#include "Force.h"
#include <stdexcept>
#include <math.h>


using namespace impact;

// Builder

Particle::Builder::Builder(const real mass)
    :
    _inverseMass{1/mass},
    _vPosition(),
    _vVelocity()
{
  if (mass == 0.0)
    throw std::invalid_argument("Illegal arugment: mass cannot be zero");
}

Particle::Builder Particle::Builder::position(const Vector3 vPosition) {
  _vPosition = vPosition;
  return *this;
}

Particle::Builder Particle::Builder::velocity(const Vector3 vVelocity) {
    _vVelocity = vVelocity;
    return *this;
}


Particle Particle::Builder::build() {
  return Particle(this->_vPosition, this->_vVelocity, this->_inverseMass);
}

// Particle

Particle::Particle(const Vector3& vPosition,
                   const Vector3& vVelocity,
                   const real inverseMass)
    :
    _vPosition(vPosition),
    _vPositionOld(vPosition),
    _vVelocity(vVelocity),
    _vForce(),
    _inverseMass{inverseMass}
{}

void Particle::applyForce(std::shared_ptr<AbstractForce> const force) {
  _vForce.addForce(force);
}

void Particle::integrate(const real time) {
  if (time <= 0) throw std::invalid_argument("Time can't be negative");

  Vector3 temp = _vPosition;

  Vector3 _vAcceleration = _inverseMass * _vForce.getResultingForce();
  _vPosition = _vPosition * 2 - _vPositionOld + _vAcceleration * (time * time);
  _vVelocity = (_vPosition - _vPositionOld) / (2 * time);

  _vPositionOld = temp;
}

real Particle::getReducedMass(const Particle& p) const {
  return 1 / (_inverseMass + p._inverseMass);
}

Vector3 const& Particle::getPosition() const {
  return _vPosition;
}

Vector3 Particle::distance(const Particle& p) const {
  return _vPosition - p._vPosition;
}

Vector3 Particle::velocityDifference(const Particle& p) const {
  return _vVelocity - p._vVelocity;
}

void Particle::updateMessage(const Mediator* em) {
  em->sendMessage(this);
}

void Particle::zeroAccelaration() {
  _vForce.clear();
}

std::string Particle::toString(size_t n) {
  return "p" + _vPosition.toString(n)+
         " v" + _vVelocity.toString(n) +
         " a" + (_vForce.getResultingForce() * _inverseMass).toString(n) +
         " m(" + utility::realFormat(1/_inverseMass, n) + ")";
}
