#ifndef BARFORCE_H
#define BARFORCE_H

#include "AbstractForce.h"

namespace impact {

class BarForce : public AbstractForce
{

 public:
  BarForce(Vector3 distance,  real reducedMass);
  void updateDistance(const Vector3& v);
  void updateVelocity(const Vector3& v);
  Vector3 const getResultingForce() const;

 private:
  Vector3 _distance, _deltaVelocity;
  Vector3 _actualDistance;
  const real _cs, _cd;
  const real _mRed;
};

}

#endif // BARFORCE_H
