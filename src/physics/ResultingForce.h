// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_RESULTING_FORCE_H_
#define SRC_RESULTING_FORCE_H_

#include "Vector3.h"
#include "AbstractForce.h"
#include <list>
#include <memory>

namespace impact {

class ResultingForce : public AbstractForce {
public:
  ResultingForce();
  void addForce(std::shared_ptr<AbstractForce> const f);
  void removeForce(std::shared_ptr<AbstractForce> f);
  Vector3 const getResultingForce() const;
  void clear();

private:
  mutable Vector3 _vResForce;
  std::list<std::shared_ptr<AbstractForce>> _forceList;
};

}

#endif // SRC_RESULTING_FORCE_H_
