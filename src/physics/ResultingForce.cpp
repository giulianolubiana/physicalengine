// Copyright [2017] <Fiorenti/Lubiana>

#include "ResultingForce.h"
#include "Force.h"

using namespace impact;

ResultingForce::ResultingForce()
    :
    _vResForce(),
    _forceList()
{}

void ResultingForce::addForce(std::shared_ptr<AbstractForce> const f) {
  _forceList.push_back(f);
}

void ResultingForce::removeForce(std::shared_ptr<AbstractForce> f) {
  _forceList.remove(f);
}

Vector3 const ResultingForce::getResultingForce() const {
  _vResForce = Vector3();
  for (auto& f : _forceList) 
    _vResForce = _vResForce + f->getResultingForce();
  
  return _vResForce;
}

void ResultingForce::clear() {
  _forceList.clear();
}
