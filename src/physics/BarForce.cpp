#include "BarForce.h"
#include <stdlib.h>
#include <iostream>
#include <stdexcept>

using namespace impact;

BarForce::BarForce(Vector3 distance, real reducedMass) :
  _distance(distance),
  _deltaVelocity(),
  _actualDistance(distance),
  _cs{ 1 },
  _cd{ 1 },
  _mRed{ reducedMass }
{
  if(distance.magnitude() == 0)
    throw std::invalid_argument("Bar length cannot be zero");
}

void BarForce::updateDistance(const Vector3& actualDistance){
  _actualDistance = actualDistance;
}

void BarForce::updateVelocity(const Vector3& v) {
  _deltaVelocity = v;
}

const Vector3 BarForce::getResultingForce() const {
  real time = utility::integrationTime;
  real ks = _mRed / (time * time);
  real deltaX = _actualDistance.magnitude() - _distance.magnitude();
  real deltaV = (_actualDistance * _deltaVelocity) / _actualDistance.magnitude();
  real kd = _mRed / time;
  return  (-ks * deltaX * _cs - kd * deltaV * _cd) *
           (_actualDistance / _actualDistance.magnitude());
}

