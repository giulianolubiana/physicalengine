#ifndef BARCONSTRAIN_H_
#define BARCONSTRAIN_H_

#include <utility>
#include "Particle.h"
#include "BarForce.h"

namespace impact {

class BarMediator  : public Mediator
{
public:
  BarMediator(Particle &part1, Particle &part2);
  void sendMessage(Collegue*) const;

private:
  std::shared_ptr<BarForce> _bf1, _bf2;
  Particle& _p1;
  Particle& _p2;

  bool correctCollegue(const Collegue* const c) const;
};

}

#endif // BARCONSTRAIN_H_
