// Copyright [2017] <Fiorenti/Lubiana>

#include "Utility.h"
#include <stdexcept>

using namespace impact;

std::string utility::realFormat(real f, size_t n) {
  const std::size_t MAX_DEC_LEN = 6;
  if (n > MAX_DEC_LEN) n = MAX_DEC_LEN;
  
  int trunc = MAX_DEC_LEN - n;
  std::string str = std::to_string(f);
  trunc = str.length() - trunc;
  str = str.substr(0, trunc);
  return str;
}

real utility::integrationTime = 0.1;

void utility::setIntegrationTime(const real time) {
  if (time <= 0) throw std::invalid_argument("Time can't be negative");
  utility::integrationTime = time;
}
