// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_ELASTICMEDIATOR_H_
#define SRC_ELASTICMEDIATOR_H_

#include <utility>
#include "Particle.h"
#include "ElasticForce.h"

namespace impact {
class ElasticForce;
class ElasticMediator : public Mediator {
public:
  ElasticMediator(ElasticForce& ef, Particle& p1, Particle& p2);
  void sendMessage(Collegue* c) const;
  
private:
  std::shared_ptr<ElasticForce> _ef1, _ef2;
  Particle& _p1;
  Particle& _p2;
  
  bool correctCollegue(const Collegue* const c) const;
};

}

#endif    // SRC_ELASTICMEDIATOR_SRCx
