// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_FORCE_H_
#define SRC_FORCE_H_

#include "AbstractForce.h"
#include "Vector3.h"

namespace impact {

class Force : public AbstractForce{
public:
  Force(Vector3 vResForce);
  Vector3 const getResultingForce() const;
private:
  Vector3 _vResForce;
};

}

#endif   // SRC_FORCE_H_
