// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_ELASTIC_FORCE_H_
#define SRC_ELASTIC_FORCE_H_

#include "AbstractForce.h"

namespace impact {

class ElasticForce : public AbstractForce {
public:
  ElasticForce(real k, real restLength);
  void updateLength(const Vector3& v);
  Vector3 const getResultingForce() const;

private:
  real _restLength;
  real _extensionLength;
  real _k;
  mutable Vector3 _force;
  Vector3 _vD;
};

}

# endif    // SRC_ELASTIC_FORCE_H_

