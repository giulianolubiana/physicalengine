// Copyright [2017] <Fiorenti/Lubiana>

#include "ElasticForce.h"

using namespace impact;

ElasticForce::ElasticForce(real k, real restLength):
    _restLength(restLength),
    _extensionLength(restLength),
    _k{ k }
{}

void ElasticForce::updateLength(const Vector3& v) {
  _extensionLength = v.magnitude();
  _vD = v;
  _vD.normalize();
}

Vector3 const ElasticForce::getResultingForce() const {
  real deltaX{ _extensionLength - _restLength };
  deltaX *= _k;
  _force = -deltaX * _vD;
  return _force;
}
