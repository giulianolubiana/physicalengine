// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_PARTICLE_H_
#define SRC_PARTICLE_H_

#include "Vector3.h"
#include "Mediator.h"
#include "ResultingForce.h"
#include "Force.h"
#include "Collegue.h"
#include <memory>

namespace impact {

class Particle : public Collegue {
 public:
    class Builder {
    public:
        Builder(const real mass);
        Builder position(const Vector3 vPosition);
        Builder velocity(const Vector3 vVelocity);
        Particle build();

    private:
        real _inverseMass;
        Vector3 _vPosition;
        Vector3 _vVelocity;
    };

    void applyForce(std::shared_ptr<AbstractForce> const force);
    void integrate(const real time);
    real getReducedMass(const Particle& p) const;
    Vector3 const& getPosition() const;
    Vector3 distance(const Particle& p) const;
    Vector3 velocityDifference(const Particle& p) const;
    void updateMessage(const Mediator* em);
    void zeroAccelaration(); 
    std::string toString(size_t n);

private:
    Particle(const Vector3& vPosition, const Vector3& vVelocity, const real inverseMass);
    
    Vector3 _vPosition;
    Vector3 _vPositionOld;
    Vector3 _vVelocity;
    ResultingForce _vForce;
    real _inverseMass;
};

}

#endif   // SRC_PARTICLE_H_
