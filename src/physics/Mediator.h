#ifndef MEDIATOR_H_
#define MEDIATOR_H_

#include "Collegue.h"

namespace impact {
class Collegue;
class Mediator {
 public:
  virtual ~Mediator() = 0;
  virtual void sendMessage(Collegue*) const = 0;

 protected:
  virtual bool correctCollegue(const Collegue* const c) const = 0;
};

 inline Mediator::~Mediator() {}
}

#endif // MEDIATOR_H_
