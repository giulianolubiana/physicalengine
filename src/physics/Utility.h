// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_UTILITY_H_
#define SRC_UTILITY_H_

#include <string>

namespace impact {
  typedef double real;

  namespace utility {
    extern real integrationTime;
    void setIntegrationTime(const real time);

    std::string realFormat(real f, size_t n);
  }
}

#endif   // SRC_UTILITY_H_
