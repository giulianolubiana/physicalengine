// Copyright [2017] <Fiorenti/Lubiana>

#ifndef SRC_VECTOR3_H_
#define SRC_VECTOR3_H_
#include <string>
#include <glm/common.hpp>
#include "Utility.h"

namespace impact {
  class Vector3;
  Vector3 operator+(const Vector3& v1, const Vector3& v2);
  Vector3 operator-(const Vector3& v1, const Vector3& v2);
  Vector3 operator*(const Vector3& v, const real scalar);
  Vector3 operator*(const real scalar, const Vector3& v);
  Vector3 operator/(const Vector3& v, const real scalar);
  real operator*(const Vector3& v1, const Vector3& v2);
}

class impact::Vector3 {
 public:
  Vector3();
  Vector3(real _x, real _y, real _z);

  void invert();
  void normalize();
  real magnitude() const;
  real squareMagnitude() const;
  bool overlap(const Vector3& v) const;
  friend Vector3 operator+(const Vector3& v1, const Vector3& v2);
  void operator+=(const Vector3& v);
  friend Vector3 operator-(const Vector3& v1, const Vector3& v2);
  void operator-=(const Vector3& v);
  Vector3 componentProduct(const Vector3& v) const;
  friend Vector3 operator*(const Vector3& v, const real scalar);
  friend Vector3 operator*(const real scalar, const Vector3& v);
  friend Vector3 operator/(const Vector3& v, const real scalar);
  friend real operator*(const Vector3& v1, const Vector3& v2);
  Vector3 vectorProduct(const Vector3& v) const;
  std::string toString(size_t n) const;
  glm::vec3 getGlmVec3() const;

 private:
  real _x;
  real _y;
  real _z;
};


#endif   // SRC_VECTOR3_H_
