// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include "gtest/gtest.h"
#include "../src/graphics/MeshImporter.h"
#include "../src/graphics/Drawer.h"

using namespace impact;

std::string meshPath = "../models/sphere_uv.ply";

TEST(MeshImporter, MeshImporterNoThrow) {
  MeshImporter mi(meshPath);
  EXPECT_NO_THROW(mi.importMesh());

  ASSERT_EQ(240, mi.getFacets().size());
  ASSERT_EQ(240, mi.getNormals().size());
  ASSERT_EQ(240, mi.getUVs().size());
  ASSERT_EQ(240, mi.getVertices().size());
}

TEST(MeshImporter, MeshWrapperConstructionException) {
  EXPECT_NO_THROW(meshWrap mw(meshPath));
}

TEST(MeshImporter, MeshWrapperConstructor) {
  meshWrap mw(meshPath);

  ASSERT_EQ(240, mw._meshImporter.getFacets().size());
  ASSERT_EQ(240, mw._meshImporter.getNormals().size());
  ASSERT_EQ(240, mw._meshImporter.getUVs().size());
  ASSERT_EQ(240, mw._meshImporter.getVertices().size());
}

TEST(MeshImporter, MeshImporterThrow) {

  MeshImporter mi("dummystring");
  try {
    mi.importMesh();
  }
  catch (std::invalid_argument& excp) {
    ASSERT_EQ("Cannot open mesh dummystring", std::string(excp.what()));
  }
}
