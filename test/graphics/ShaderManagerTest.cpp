// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include "gtest/gtest.h"
#include "../src/graphics/ShaderManager.h"
#include <GLFW/glfw3.h>

using namespace impact;

std::string shadersFolder = "../src/graphics/shaders/";

void windowInitialization() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  GLFWwindow* _window = glfwCreateWindow( 1024, 768, "Test", NULL, NULL);
  glfwMakeContextCurrent(_window);

  glewExperimental = true; // Needed for core profile
  glewInit();
}

TEST(ShaderManager, ShaderManagerCorrect) {
  windowInitialization();
  ShaderManager sm;

  sm.init();
  sm.addShader(GL_VERTEX_SHADER, shadersFolder+"meshlab_model_vertex_shader.glsl");
  sm.addShader(GL_FRAGMENT_SHADER, shadersFolder+"meshlab_model_fragment_shader.glsl");
  sm.finalize();
  sm.enable();
}

TEST(ShaderManager, ShaderManagerWrongPath) {
  windowInitialization();
  ShaderManager sm;

  sm.init();
  EXPECT_THROW(sm.addShader(GL_VERTEX_SHADER, "dummy"), std::runtime_error);
  EXPECT_THROW(sm.addShader(GL_FRAGMENT_SHADER, "dummy"), std::runtime_error);
  EXPECT_THROW(sm.finalize(), std::runtime_error);
}

TEST(ShaderManager, ShaderManagerCompileError) {
  windowInitialization();
  ShaderManager sm;

  sm.init();
  EXPECT_THROW(sm.addShader(GL_VERTEX_SHADER, shadersFolder+"dummy_shader.glsl"), std::runtime_error);
  EXPECT_THROW(sm.addShader(GL_FRAGMENT_SHADER, shadersFolder+"dummy_shader.glsl"), std::runtime_error);
}

TEST(ShaderManager, ShaderManagerPrintUniforms) {
  windowInitialization();
  ShaderManager sm;

  sm.init();
  sm.addShader(GL_VERTEX_SHADER, shadersFolder+"meshlab_model_vertex_shader.glsl");
  sm.addShader(GL_FRAGMENT_SHADER, shadersFolder+"meshlab_model_fragment_shader.glsl");
  sm.finalize();
  sm.enable();

  std::ostringstream oss;
  std::streambuf* p_cout_streambuf = std::cout.rdbuf();
  std::cout.rdbuf(oss.rdbuf());

  sm.printActiveUniforms();
  ASSERT_EQ(oss.str(), "Uniform number 0: MVP\nUniform number 1: V\nUniform number 2: M\n");

  ShaderManager empty;
  empty.init();
  oss.str("");

  empty.printActiveUniforms();
  ASSERT_EQ(oss.str(), "No uniforms\n");

  std::cout.rdbuf(p_cout_streambuf); // restore
}

TEST(ShaderManager, ShaderManagerGetVariables) {
  windowInitialization();
  ShaderManager sm;

  sm.init();
  sm.addShader(GL_VERTEX_SHADER, shadersFolder+"meshlab_model_vertex_shader.glsl");
  sm.addShader(GL_FRAGMENT_SHADER, shadersFolder+"meshlab_model_fragment_shader.glsl");
  sm.finalize();
  sm.enable();

  ASSERT_EQ(sm.getAttribLocation("position"), 0);
  ASSERT_EQ(sm.getUniformLocation("MVP"), 0);
  ASSERT_THROW(sm.getUniformLocation("dummy"), std::invalid_argument);
}

TEST(ShaderManager, ShaderManagerIncorrectFinalize) {
  windowInitialization();
  ShaderManager sm;

  ASSERT_THROW(sm.finalize(), std::runtime_error);
}
