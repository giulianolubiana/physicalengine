// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include <vector>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "gtest/gtest.h"
#include "../src/graphics/Drawer.h"
#include "../src/physics/Particle.h"
#include "../src/physics/ElasticForce.h"
#include "../src/physics/ElasticMediator.h"
#include "../src/physics/Force.h"

using namespace impact;

TEST(DrawerTests, DrawerConstructor) {
  Particle par = Particle::Builder(2.0).position(Vector3(0,0,0))
                                        .velocity(Vector3(0,0,0))
                                        .build();

  Particle par2 = Particle::Builder(2.0).position(Vector3(0,0,0))
                                        .velocity(Vector3(0,0,0))
                                        .build();

  
  par.applyForce(std::make_shared<Force>(Vector3(10.0, 10.0, 0.0) * 2));
  par2.applyForce(std::make_shared<Force>(Vector3(10.0, 0.0, 0.0) * 2));

  std::pair<std::string, Particle*> paio = std::make_pair("../models/sphere_uv.ply", &par2);

  EXPECT_NO_THROW(Drawer draw);

  Drawer draw;

  std::vector<std::pair<std::string, Particle*>> meshPosesVector;
  meshPosesVector.push_back(std::make_pair("../models/sphere_uv.ply", &par));
  draw.setPairs(meshPosesVector);
  draw.addPair(paio);

  ASSERT_EQ(draw.renderOnScreen(), true);
  par.integrate(0.01);
  par2.integrate(0.01);
  draw.closeWindow(); // Here we simulate the action of pressing ESC or Alt+F4

  ASSERT_EQ(draw.renderOnScreen(), false);
}


