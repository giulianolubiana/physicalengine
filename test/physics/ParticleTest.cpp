// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include <memory>
#include "gtest/gtest.h"
#include "../src/physics/Force.h"
#include "../src/physics/Particle.h"

using namespace impact;

Vector3 p(1.0f, 2.0f, 3.0f);
Vector3 v(5.3f, 2.0f, 1.234f);
Vector3 a(4.24f, 9.12f, 14.5f);

TEST(ParticleTests, ToString) {
  Particle par = Particle::Builder(1.0f).build();
  ASSERT_EQ("p(0.0, 0.0, 0.0) v(0.0, 0.0, 0.0) a(0.0, 0.0, 0.0) m(1.0)",
            par.toString(1));

  EXPECT_NO_THROW(Particle::Builder(1.0f).build());
}

TEST(ParticleTests, ZeroMass) {
    ASSERT_THROW(Particle::Builder(0.0f).build(), std::invalid_argument);

    try {
        Particle::Builder(0.0f).build();
    } catch(std::invalid_argument& exp) {
        std::string str(exp.what());

        ASSERT_EQ("Illegal arugment: mass cannot be zero", str);
    }
}

TEST(ParticleTests, Builders) {
    Particle par = Particle::Builder(1.0f).position(p)
                                          .velocity(v)
                                          .build();
    ASSERT_EQ("p(1.0, 2.0, 3.0) v(5.3, 2.0, 1.2) a(0.0, 0.0, 0.0) m(1.0)",
              par.toString(1));
}



TEST(ParticleTests, Integrator) {
  Particle par = Particle::Builder(2.0f).position(p)
                                        .velocity(v)
                                        .build();
  
  par.applyForce(std::make_shared<Force>(a * 2));
  par.integrate(2.1f);
  ASSERT_EQ("p(19.6, 42.2, 66.9) v(4.4, 9.5, 15.2) a(4.2, 9.1, 14.5) m(2.0)",
            par.toString(1));
}

TEST(ParticleTests, NegativeTime) {
  Particle par = Particle::Builder(2.0f).position(p)
                                        .velocity(v)
                                        .build();
  ASSERT_THROW(par.integrate(-12), std::invalid_argument);

  try {
    par.integrate(-12);
  }
  catch (std::invalid_argument& excp) {
    ASSERT_EQ("Time can't be negative", std::string(excp.what()));
  }
}

TEST(ParticleTests, ParticleDistance) {
  Vector3 pos1 = Vector3(1.0f, 2.0f, 3.0f);
  Vector3 pos2 = Vector3(12.4f, -7.2f, 0.0f);
  Particle part1 = Particle::Builder(2.0f).position(pos1).build();
  Particle part2 = Particle::Builder(2.0f).position(pos2).build();

  ASSERT_EQ("(-11.4, 9.2, 3.0)", part1.distance(part2).toString(1));
}
