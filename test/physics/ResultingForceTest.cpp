// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include <memory>
#include "gtest/gtest.h"
#include "../src/physics/Particle.h"
#include "../src/physics/ResultingForce.h"
#include "../src/physics/Force.h"

using namespace impact;

TEST(ForceTests, ForceConstructor) {
  Force randomForce = Force(Vector3(0.0f, 5.0f, 0.0f));

  ASSERT_EQ("(0.0, 5.0, 0.0)", randomForce.getResultingForce().toString(1));
}

TEST(ForceTests, ParticleWith) {
    Particle par = Particle::Builder(2.0f)
                             .position(Vector3(0.0f, 0.0f, 0.0f))
                             .build();

    par.applyForce(std::make_shared<Force>(Vector3(10.0, 0.0, 0.0)));
    par.integrate(1);

    ASSERT_EQ("p(5.0, 0.0, 0.0) v(2.5, 0.0, 0.0) a(5.0, 0.0, 0.0) m(2.0)",
              par.toString(1));
}

TEST(ForceTests, ResultinNullForcesTest) {
    Particle par = Particle::Builder(2.0f)
                             .position(Vector3(0.0f, 0.0f, 0.0f))
                             .build();

    ResultingForce vResForce;
    vResForce.addForce(std::make_shared<Force>(Vector3(10.0f, 0.0f, 0.0f)));
    vResForce.addForce(std::make_shared<Force>(Vector3(-10.0f, 0.0f, 0.0f)));

    par.applyForce(std::make_shared<ResultingForce>(vResForce));
    par.integrate(1);

    ASSERT_EQ("p(0.0, 0.0, 0.0) v(0.0, 0.0, 0.0) a(0.0, 0.0, 0.0) m(2.0)",
              par.toString(1));
}

TEST(ForceTests, ResultinNegativeForcesTest) {
    Particle par = Particle::Builder(2.0f)
                             .position(Vector3(0.0f, 0.0f, 0.0f))
                             .build();
    auto vPosForce = std::make_shared<Force>(Vector3(10.0f, 0.0f, 0.0f));
    auto vNegForce = std::make_shared<Force>(Vector3(-10.0f, 0.0f, 0.0f));
    ResultingForce vResForce;
    
    vResForce.addForce(vPosForce);
    vResForce.addForce(vNegForce);
    vResForce.removeForce(vPosForce);

    par.applyForce(std::make_shared<ResultingForce>(vResForce));
    par.integrate(1);

    ASSERT_EQ("p(-5.0, 0.0, 0.0) v(-2.5, 0.0, 0.0) a(-5.0, 0.0, 0.0) m(2.0)",
              par.toString(1));
}
