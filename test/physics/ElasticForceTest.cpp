// Copyright [2017] <Fiorenti/Lubiana>

#include <stdexcept>
#include "gtest/gtest.h"
#include "../src/physics/ElasticForce.h"
#include "../src/physics/Collegue.h"
#include "../src/physics/ElasticMediator.h"
#include "../src/physics/AbstractForce.h"

using namespace impact;

static Vector3 p1(1.0, 2.0, 1.0);
static Vector3 p2(5.0, 2.0, 1.0);
static Vector3 ve(5.3, 2.0, 1.234);
static Vector3 ae(4.24, 9.12, 14.5);


TEST(ElasticForceTests, ParticleExtention) {
  Particle part1 = Particle::Builder(1.0).position(p1).build();
  Particle part2 = Particle::Builder(1.0).position(p2).build();
  
  ElasticForce spring(12.0, 2.0);
  ElasticMediator em(spring, part1, part2);

  part1.updateMessage(&em);
  part2.updateMessage(&em);
  
  part1.integrate(0.5);
  part2.integrate(0.5);
  
  ASSERT_EQ("p(7.0, 2.0, 1.0) v(6.0, 0.0, 0.0) a(24.0, 0.0, 0.0) m(1.0)",
            part1.toString(1));
  ASSERT_EQ("p(-1.0, 2.0, 1.0) v(-6.0, 0.0, 0.0) a(-24.0, 0.0, 0.0) m(1.0)",
            part2.toString(1));
}


TEST(ElasticForceTests, CompressionTest) {
  Particle part1 = Particle::Builder(1.0).position(p1).build();
  Particle part2 = Particle::Builder(1.0).position(p2).build();

  ElasticForce spring(12.0, 10.0);
  ElasticMediator em(spring, part1, part2);

  part1.updateMessage(&em);
  part2.updateMessage(&em);
  
  part1.integrate(0.5);
  part2.integrate(0.5);

  ASSERT_EQ("p(-17.0, 2.0, 1.0) v(-18.0, 0.0, 0.0) a(-72.0, 0.0, 0.0) m(1.0)",
            part1.toString(1));
  ASSERT_EQ("p(23.0, 2.0, 1.0) v(18.0, 0.0, 0.0) a(72.0, 0.0, 0.0) m(1.0)",
            part2.toString(1));
}

TEST(ElasticForceTests, CollegueException) {
  Particle part1 = Particle::Builder(1.0).position(p1).build();
  Particle part2 = Particle::Builder(1.0).position(p2).build();
  Particle part3 = Particle::Builder(1.0).position(p2).build();

  ElasticForce spring(12.0, 10.0);
  ElasticMediator em(spring, part1, part2);
  ASSERT_THROW(part3.updateMessage(&em), std::invalid_argument);
}
