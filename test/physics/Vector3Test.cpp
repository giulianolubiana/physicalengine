// Copyright [2017] <Fiorenti/Lubiana>

#include "gtest/gtest.h"
#include "../src/physics/Vector3.h"

using namespace impact;

TEST(Vector3Test, Vector3ToStringTest) {
  Vector3 vec(1.0f, 2.0f, 3.0f);
  ASSERT_EQ("(1.000000, 2.000000, 3.000000)", vec.toString(50));
}

TEST(Vector3Test, VectorDefaultConstructorTest) {
  Vector3 vec;

  ASSERT_EQ("(0.0, 0.0, 0.0)", vec.toString(1));
}

TEST(Vector3Test, VectorThreeFloatParametersConstructorTest) {
  Vector3 vec(1.0f, 2.0f, 3.0f);

  ASSERT_EQ("(1.0, 2.0, 3.0)", vec.toString(1));
}

TEST(Vector3Test, InvertFunctionTest) {
  Vector3 vec(1.0f, 2.0f, 3.0f);
  vec.invert();
  ASSERT_EQ("(-1.0, -2.0, -3.0)", vec.toString(1));
}

TEST(Vector3Test, MagnitudeFunctionTest) {
  Vector3 vec(4.0f, 3.0f, 0.0f);

  ASSERT_EQ(5.0f, vec.magnitude());
}

TEST(Vector3Test, SquareMagnitudeFunctionTest) {
  Vector3 vec(4.0f, 3.0f, 2.0f);

  ASSERT_EQ(29.0f, vec.squareMagnitude());
}

TEST(Vector3Test, ScalarProductFunctionTest) {
  Vector3 vec(4.0f, 3.0f, 2.0f);
  ASSERT_EQ("(8.0, 6.0, 4.0)", (vec * 2.0).toString(1));
  ASSERT_EQ("(8.0, 6.0, 4.0)", (2.0 * vec).toString(1));
}

TEST(Vector3Test, ScalarDivisionFunctionTest) {
  Vector3 vec(4.0f, 3.0f, 2.0f);
  ASSERT_EQ("(2.0, 1.5, 1.0)", (vec / 2.0).toString(1));
}

TEST(Vector3Test, NormalizeFunctionTest) {
  Vector3 vec(4.0f, 3.0f, 0.0f);
  vec.normalize();
  ASSERT_EQ("(0.8, 0.6, 0.0)", vec.toString(1));
}

TEST(Vector3Test, AdditionTest) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  ASSERT_EQ("(2.0, 7.0, -2.0)", (vec1 + vec2).toString(1));
}

TEST(Vector3Test, SelfAdditionTest) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  vec1 += vec2;
  ASSERT_EQ("(2.0, 7.0, -2.0)", vec1.toString(1));
}

TEST(Vector3Test, SubtractionTest) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  ASSERT_EQ("(6.0, -3.0, 4.0)", (vec1 - vec2).toString(1));
}

TEST(Vector3Test, SelfSubtractionTest) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  vec1 -= vec2;
  ASSERT_EQ("(6.0, -3.0, 4.0)", vec1.toString(1));
}

TEST(Vector3Test, ComponentProductTest) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  ASSERT_EQ("(-8.0, 10.0, -3.0)", vec1.componentProduct(vec2).toString(1));
}

TEST(Vector3Test, TestProductScalar) {
  Vector3 vec1(4.0f, 2.0f, 1.0f);
  Vector3 vec2(-2.0f, 5.0f, -3.0f);
  ASSERT_EQ(-1, vec1 * vec2);
}

TEST(Vector3Test, NormalizeNullVectorTest) {
  Vector3 vec(0.0f, 0.0f, 0.0f);
  vec.normalize();
  ASSERT_EQ("(0.0, 0.0, 0.0)", vec.toString(1));
}

TEST(Vector3Test, VectorProductTest) {
  Vector3 vec1(1.0f, 2.0f, 3.0f);
  Vector3 vec2(3.0f, 4.0f, 5.0f);
  ASSERT_EQ("(-2.0, 4.0, -2.0)", vec1.vectorProduct(vec2).toString(1));
}
