// Copyright [2017] <Fiorenti/Lubiana>

#include "gtest/gtest.h"
#include "../src/physics/Particle.h"
#include "../src/physics/BarMediator.h"

using namespace impact;

const real DELTA_DISTANCE = 0.0001;
Vector3 pos1(1.0, 0.0, 0.0);
Vector3 vel1;
Vector3 pos2(1.2, 0.3, 2.3);
Vector3 vel2;

TEST(BarMediator, NoForces) {
  Particle par1 = Particle::Builder(1.0).position(pos1)
                                        .velocity(vel1)
                                        .build();

  Particle par2 = Particle::Builder(1.0).position(pos2)
                                        .velocity(vel2)
                                        .build();

  BarMediator bar(par1, par2);
  real distanceBefore = par1.distance(par2).magnitude();
  par1.updateMessage(&bar);
  par2.updateMessage(&bar);

  utility::setIntegrationTime(0.01);
  par1.integrate(utility::integrationTime);
  par2.integrate(utility::integrationTime);
  
  real distanceAfter = par1.distance(par2).magnitude();

  EXPECT_LE(fabs(distanceBefore - distanceAfter), DELTA_DISTANCE);
}

TEST(BarMediator, SecondTest) {
  Particle par1 = Particle::Builder(1.0).position(pos1)
                                        .velocity(vel1)
                                        .build();

  Particle par2 = Particle::Builder(1.0).position(pos2)
                                        .velocity(vel2)
                                        .build();

  BarMediator bar(par1, par2);
  par1.applyForce(std::make_shared<Force>(Vector3(0.0, 1.0, 0.0)));
  real distanceBefore = par1.distance(par2).magnitude();
  utility::setIntegrationTime(0.01);

  par1.integrate(utility::integrationTime);
  par2.integrate(utility::integrationTime);

  par1.updateMessage(&bar);
  par2.updateMessage(&bar);

  par1.integrate(utility::integrationTime);
  par2.integrate(utility::integrationTime);

  real distanceAfter = par1.distance(par2).magnitude();

  EXPECT_LE(fabs(distanceBefore - distanceAfter), DELTA_DISTANCE);
}

TEST(BarMediator, DistanceZero) {
  Particle par1 = Particle::Builder(1.0).position(Vector3())
                                        .velocity(Vector3())
                                        .build();

  Particle par2 = Particle::Builder(1.0).position(Vector3())
                                        .velocity(Vector3())
                                        .build();

  ASSERT_THROW(BarMediator bar(par1, par2), std::invalid_argument);
}
