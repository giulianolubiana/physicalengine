# Impact Physics Engine

**Impact** is an *energy based* physics engine based on the work done
by [ETH Zurich](https://www.ethz.ch/en.html).

With the physics engine a very basic graphic engine has been
developed, using [OpenGL](https://www.opengl.org/). The aim of the
graphics engine is simply to show on screen the physics simulation
done by **impact** and for no means is it the only graphic engine that
you can use. The graphic engine is completely decoupled from
**impact**, so, if you wish, you can use your own.

## External libraies
This project makes use of the
[googletest](https://github.com/google/googletest) testing library and
of the [GSL: Guidelines Support
Library](https://github.com/Microsoft/GSL) that contains functions and
types that are suggested for use by the [C++ Core
Guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
maintained by the [Standard C++ Foundation](https://isocpp.org/).

The graphics engine also uses the [OpenGL](https://www.opengl.org/) graphic library.

## Quick Start
### Supported Platforms

The test suite and sample that exercise **impact** have been built and
passed on successfully to the following platforms:

* GNU/Linux using LLVM/Clang 3.8.0
* GNU/Linux using GCC 5.1

### Building the tests and samples
To build the tests and the samples, you will require the following:

* [Cmake](https://cmake.org/) version 3.1.3 or later to be installed and in your PATH.

These steps assume the source code of this repository have been cloned into a directory named ```physicalengine```:

1. Create a directory to contain the build outputs

        cd physicalengine
        mkdir build
        cd build

2. Configure CMake to use the compiler of your choice (you can see a list by running `cmake --help`).

        cmake -G Ninja ..

3. Build the test suite and the samples.

        ninja

4. Run the test suite.

        ./test/alltests

All tests should pass - indicating your platform is fully supported and you are ready to use the **impact** physics engine.

### Trying the samples
All the samples can be found in the ```./samples``` folder. There are two kinds of samples:

1. ```mesh_command_line```;
2. all the others.

To execute ```mesh_command_line``` you have to invoke it as:

       ./samples/mesh_command_line mesh.msh N

where ```mesh.msh``` is a mesh built using [Mesh
Genrator](http://gmsh.info/) and ```N = 2``` if ```mesh.msh```
represents a 2D object and ```N = 3``` if it represents a 3D
object. You can find some ```.msh``` files in
```../test/meshImporter```.

So, you can use `mesh_command_line` as:

    ./samples/mesh_command_line ../test/meshImporter/spheere2.msh 3

All the other samples don't need any argument and you can simply invoke them, as:

     ./samples/spring

### Current State of the project
Currently the project is ongoing. The simulation of forces for 1, 2 and 3 dimension objects have been implemented; it can be found on the [develop](https://bitbucket.org/giulianolubiana/physicalengine/src/develop/) branch.

At the present moment we are implementing the collision detection part that can be found on the [collisionDetection](https://bitbucket.org/giulianolubiana/physicalengine/src/collisionDetection/) branch.

## References
1. [A versatile and robust model for geometrically complex deformable solids](https://ieeexplore.ieee.org/document/1309227)
2. [Optimized Spatial Hashing for Collision Detection of Deformable Objects](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.4.5881)
3. [Consistent Penetration Depth Estimation for Deformable Collision Response](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.87.3440)
4. [Contact Handling for Deformable Point-Based Objects](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.515.8415&rank=1)
5. [OpenGL Insights: Asynchronous Buffer Transfers](https://www.seas.upenn.edu/~pcozzi/OpenGLInsights/OpenGLInsights-AsynchronousBufferTransfers.pdf).
6. [Honors Calculus III/IV](https://www.owlnet.rice.edu/~fjones/).
